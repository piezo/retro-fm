//
//  RFMCommonData.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMCommonData.h"

@implementation RFMCommonData

+ (instancetype)shared
{
    static dispatch_once_t onceQueue;
    static RFMCommonData *rFMCommonData = nil;
    
    dispatch_once(&onceQueue, ^{ rFMCommonData = [[self alloc] init]; });
    return rFMCommonData;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.selectedBitrate = 64.0;
        self.player = [[STKAudioPlayer alloc] init];
    }
    return self;
}


-(void) play{
    NSString *url;
    
    switch ((int) [[RFMCommonData shared] selectedBitrate] ) {
        case 32:
        {
            url = @"http://retro32.streamr.ru";
            break;
        }
            
        case 64:
        {
            url = @"http://retro64.streamr.ru";
            break;
        }
            
        case 128:
        {
            url = @"http://retro128.streamr.ru";
            break;
        }
            
        case 256:
        {
            url = @"http://retro256.streamr.ru";
            break;
        }
            
            
            
        default:
            break;
    }
    
    [self.player play:url];
}

-(void) stop{
    [self.player stop];
}


static BOOL _playlistUpdateRunning = NO;

-(void) startPlaylistUpdate{
    
    _playlistUpdateRunning = YES;
    @weakify(self);
    
    [[RACObserve(self, playlistSignal) ignore:nil] subscribeNext:^(id x) {
        [x subscribeNext:^(RACTuple *x) {
            if (_playlistUpdateRunning) {
                

                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
                [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                NSString *serverDateString =[[(AFHTTPRequestOperation *) x.first response] allHeaderFields][@"Date"];
//                NSDate *serverDate = [dateFormatter dateFromString: serverDateString];
                
                
                if ([x.second isKindOfClass:[NSArray class]]) {
                    
                    
                    NSNumber *timestamp = x.second[0][@"start"];
                    NSNumber *duration = x.second[0][@"duration"];
                    
                    NSString *artist =  x.second[0][@"artist"];
                    NSString *name =  x.second[0][@"name"];
                    
                    NSDate *startdate = [NSDate dateWithTimeIntervalSince1970:[timestamp floatValue]];
                    
                    [[RFMCommonData shared] setOnAirTitle:[NSString stringWithFormat:@"%@ - %@", artist, name]];
                    [[RFMCommonData shared] setOnAirStart:startdate];
                    [[RFMCommonData shared] setOnAirDuration:[duration floatValue]];
                    
                }
                
                @strongify(self);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self fetchPlaylist];
                });
                
            }
        } error:^(NSError *error) {
            NSLog(@"%@", error);
        }];
    }];
    
    [self fetchPlaylist];
    
    
    [[RACSignal interval:1 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        NSMutableSet *set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
        [set addObject:@"application/x-javascript"];
        manager.responseSerializer.acceptableContentTypes = set;
        
        RACSignal *get =
        [manager rac_GET:@"http://www.europaplus.ru/online/air/1.js" parameters:@{@"t":@([[NSDate new] timeIntervalSince1970])}];
        
        [get subscribeNext:^(RACTuple *x) {
            NSLog(@"%@", [x.second class]);
        } error:^(NSError *error) {
            NSLog(@"%@", error);
        }];
        
        
    }];

    
}

-(void) stopPlaylistUpdate{
    _playlistUpdateRunning = NO;
    self.playlistSignal = nil;
}


-(void) fetchPlaylist{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSMutableSet *ms = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [ms addObject:@"application/x-javascript"];
    
    manager.responseSerializer.acceptableContentTypes = ms;
    
    
    if (_playlistSignal) {
        //        [_playlistSignal dispose];
    }
    
    self.playlistSignal = [manager rac_GET:@"http://retrofm.ru/online/air/playlist.js"
                                parameters:@{
                                             @"_":@([[NSDate date] timeIntervalSince1970])
                                             }];
    
    
    
    
}


@end
