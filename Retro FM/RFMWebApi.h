//
//  RFMWebApi.h
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RFMWebApi : NSObject{
//    RACSignal *playlistSignal;
}

@property (nonatomic, strong) RACSignal *playlistSignal;

-(void) startPlaylistUpdate;
-(void) stopPlaylistUpdate;
-(void) fetchPlaylist;

@end
