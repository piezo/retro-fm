//
//  RFMRetroButton.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMRetroButton.h"

@implementation RFMRetroButton


-(void)awakeFromNib{
    
    
    @weakify(self)
    
    
    
    [[RACSignal merge:@[
                            [self rac_signalForControlEvents:UIControlEventTouchUpInside],
                            [self rac_signalForControlEvents:UIControlEventTouchDragExit],
                            [self rac_signalForControlEvents:UIControlEventTouchDown],
     
                            ]]
     subscribeNext:^(id x)
    {
        @strongify(self);
        [self setNeedsDisplay];
    }];
    
    [RACObserve(self, checked) subscribeNext:^(id x) {
        @strongify(self);
        [self setNeedsDisplay];
        if ([x boolValue]) {
            [self setTintColor:_ledColor];
            self.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.titleLabel.layer.shadowRadius = 3.0f;
            self.titleLabel.layer.shadowOpacity = 1;
            self.titleLabel.layer.shadowOffset = CGSizeZero;
            self.titleLabel.layer.masksToBounds = NO;
        }else{
            [self setTintColor:[UIColor blackColor]];
            self.titleLabel.layer.shadowRadius = 0;
            self.titleLabel.layer.shadowOpacity = 0;
        }
    }];
    
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor=[UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [RFMStyleKit drawRetroBtnWithLedColor:_ledColor
                              highlighted:self.highlighted || self.checked
                                  enabled:self.checked];
}

@end
