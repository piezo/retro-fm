//
//  RFMCommonData.h
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StreamingKit/STKAudioPlayer.h>

@interface RFMCommonData : NSObject{
}

@property (nonatomic, strong) NSString *onAirTitle;
@property (nonatomic, strong) NSDate  *onAirStart;
@property (nonatomic, assign) CGFloat onAirDuration;
@property (nonatomic, assign) CGFloat onAirProgress;
@property (nonatomic, assign) CGFloat signalQuality;
@property (nonatomic, assign) CGFloat selectedBitrate;
@property (nonatomic, strong) STKAudioPlayer *player;
@property (nonatomic, strong) RACSignal *playlistSignal;


+ (instancetype)shared;

-(void) play;
-(void) stop;

-(void) startPlaylistUpdate;
-(void) stopPlaylistUpdate;
-(void) fetchPlaylist;

@end
