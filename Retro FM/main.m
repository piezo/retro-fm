//
//  main.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RFMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RFMAppDelegate class]));
    }
}
