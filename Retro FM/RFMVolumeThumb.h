//
//  RFMVolumeThumb.h
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@interface RFMVolumeThumb : UIView{

}

@property (nonatomic, assign) CGFloat volume;
@property (nonatomic, assign) CGFloat lightAngle;
@property (nonatomic, assign) CGFloat rotationAngle;
@property (nonatomic, assign, getter=isHighlighted) BOOL highlighted;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

-(void) playBtnDidPress;

@end
