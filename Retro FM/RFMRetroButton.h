//
//  RFMRetroButton.h
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RFMRetroButton : UIButton{
    UIColor *_ledColor;
    BOOL _checked;
}

@property (nonatomic, strong) UIColor *ledColor;
@property (nonatomic, assign, getter=isChecked) BOOL checked;
@property (nonatomic, strong) NSNumber *bitrate;

@end
