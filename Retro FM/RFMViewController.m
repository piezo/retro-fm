//
//  RFMViewController.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMViewController.h"
#import "RFMSignalHUD.h"
#import "RFMRetroButton.h"
#import "RFMVolumeThumb.h"

@interface RFMViewController ()

@property (weak, nonatomic) IBOutlet RFMSignalHUD *signalHUD;
@property (strong, nonatomic) IBOutletCollection(RFMRetroButton) NSArray *bitrateBtns;
@property (weak, nonatomic) IBOutlet MPVolumeView *hVolumeView;
@property (weak, nonatomic) IBOutlet UISlider *hVolumeSlider;
@property (weak, nonatomic) IBOutlet RFMVolumeThumb *mVolumeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hVolumeLeftConstraint;

@end

@implementation RFMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
    });
    

    [_bitrateBtns enumerateObjectsUsingBlock:^(RFMRetroButton *btn,
                                               NSUInteger idx,
                                               BOOL *stop)
    {
        [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            [[RFMCommonData shared] setSelectedBitrate:[btn.bitrate floatValue]];
        }];
        
    }];
    
    @weakify(self)
    [RACObserve([RFMCommonData shared], selectedBitrate) subscribeNext:^(NSNumber* x) {
        @strongify(self);
        [self.bitrateBtns enumerateObjectsUsingBlock:^(RFMRetroButton *btn,
                                                       NSUInteger idx,
                                                       BOOL *stop)
        {
            
            btn.checked = btn.bitrate.floatValue == x.floatValue;
            
        }];
        
        if ([[[RFMCommonData shared] player] state] == STKAudioPlayerStatePlaying) {
            [[RFMCommonData shared] play];
            
            

        }

    }];

    
    RACSignal *volumeChanged = [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil]
                                map:^id(NSNotification *x) {
        
                                    return x.userInfo[@"AVSystemController_AudioVolumeNotificationParameter"];
                                }];
    
    
    RAC(self.mVolumeView, volume) = volumeChanged;
    
    [[_hVolumeView subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UISlider class]]) {
            @strongify(self);
            self.hVolumeSlider = obj;
            
            self.mVolumeView.volume = self.hVolumeSlider.value;

            RACChannelTerminal *mVolumeViewTerminal = RACChannelTo(self.mVolumeView, volume);
            RACChannelTerminal *hVolumeSliderTerminal = RACChannelTo(self.hVolumeSlider, value);
            
            [mVolumeViewTerminal subscribe:hVolumeSliderTerminal];


        }
    }];
    
    _hVolumeLeftConstraint.constant=-300;

    [[self.mVolumeView rac_signalForSelector:@selector(playBtnDidPress)] subscribeNext:^(id x) {
        @strongify(self);
        if (self.mVolumeView.selected) {
            [[RFMCommonData shared] play];
        }else{
            [[RFMCommonData shared] stop];
        }
    }];
    
    [RACObserve([RFMCommonData shared], player.state) subscribeNext:^(id x) {
        
        STKAudioPlayerState state = [x intValue];
        
        @strongify(self);
        if (state == STKAudioPlayerStateStopped) {
            self.mVolumeView.selected = NO;
            [[RFMCommonData shared] setSignalQuality:0];
        }
        
        if (state == STKAudioPlayerStatePlaying || state == STKAudioPlayerStateBuffering) {
            self.mVolumeView.selected = YES;
            
            RACSignal *s = [RACObserve([RFMCommonData shared], player.state) filter:^BOOL(id value) {
                STKAudioPlayerState state = [value intValue];
                return  state == STKAudioPlayerStateStopped;
            }];
            
            [[[RACSignal interval:.1 onScheduler:[RACScheduler mainThreadScheduler]] takeUntil:s] subscribeNext:^(id x) {
                float randomNum = ((float)rand() / RAND_MAX) *.1+.8;
                [[RFMCommonData shared] setSignalQuality:randomNum];
            }];
        }
        
    
    }];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
