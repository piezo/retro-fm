
//
//  RFMWebApi.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMWebApi.h"
#import "AFNetworking.h"

@implementation RFMWebApi

static BOOL _playlistUpdateRunning = NO;

-(void) startPlaylistUpdate{
    
    _playlistUpdateRunning = YES;
    @weakify(self);
    [self.playlistSignal subscribeNext:^(id x) {
        if (_playlistUpdateRunning) {
            
            @strongify(self);

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self fetchPlaylist];
            });

        }
    }];
    
        
}

-(void) stopPlaylistUpdate{
    _playlistUpdateRunning = NO;
    self.playlistSignal = nil;
}


-(void) fetchPlaylist{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    
    [(NSMutableSet *) manager.responseSerializer.acceptableContentTypes addObject:@"text/html"];

    
    if (_playlistSignal) {
//        [_playlistSignal dispose];
    }
    
    self.playlistSignal = [manager rac_GET:@"http://retrofm.ru/online/air/playlist.js"
                                parameters:@{
                                            @"_":@([[NSDate date] timeIntervalSince1970])
                                            }];
    
    
    
    
}


@end
