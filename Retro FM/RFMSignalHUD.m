//
//  RFMSignalHUD.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMSignalHUD.h"
#import "PRTween.h"

@implementation RFMSignalHUD





-(void)awakeFromNib{
    
    @weakify(self)

    [[[RACObserve([RFMCommonData shared], signalQuality) distinctUntilChanged] deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
        @strongify(self);
        
        PRTweenOperation *o= [PRTween tween:self property:@"mQuality" from:_mQuality to:[x floatValue] duration:1 timingFunction:PRTweenTimingFunctionBackOut
                                updateBlock:^(PRTweenPeriod *period) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self setNeedsDisplay];
                                    });
                                    
                                } completeBlock:^{
//                                    NSLog(@"%f", _mQuality);
                                }];
        o.override = YES;

    }];
    
    
    
//    [tracer start];
    
//    [[RACObserve(self, mQuality) deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
//        [self setNeedsDisplay];
//    }];
    

}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [RFMStyleKit drawSignalHUDWithSignalQuality:_mQuality];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    

    
}

@end
