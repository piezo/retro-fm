//
//  RFMOnAirTimelineView.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMOnAirTimelineView.h"
#import "RFMCommonData.h"

@interface RFMOnAirTimelineView()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) CGFloat progress;


@end

@implementation RFMOnAirTimelineView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self _init];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self _init];
    }
    return self;
}

- (void)_init
{
    
    RAC(self, title) = RACObserve([RFMCommonData shared], onAirTitle);
    RAC(self, progress) = RACObserve([RFMCommonData shared], onAirProgress);
    
    RACSignal *dataChangesSignal = [RACSignal merge:@[ [RACObserve(self, title) ignore:nil] , RACObserve(self, progress)]];
    
    [dataChangesSignal subscribeNext:^(id x) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });

    }];
    
    @weakify(self);
    [[RACSignal interval:1.0/30.0 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
       
        @strongify(self);
        if ([[RFMCommonData shared] onAirStart]) {

            CGFloat interval = [[NSDate new] timeIntervalSinceDate:[[RFMCommonData shared] onAirStart]];
            CGFloat progress = interval/[[RFMCommonData shared] onAirDuration];
            progress = progress>1?1:progress;
            progress = progress<0?0:progress;
            
            self.progress = progress;
            
        }

    }];
    
    

    
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
//    NSLog(@"%f", _progress);
//    _title = @"test";
    [RFMStyleKit drawOnairTimelineWithArtistTitleText:_title timeLineProgress:_progress];
}

@end
