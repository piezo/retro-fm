//
//  RFMStyleKit.m
//  Retro FM
//
//  Created by Alex Izvekov on 9/29/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//

#import "RFMStyleKit.h"


@implementation RFMStyleKit

#pragma mark Cache

static UIColor* _btnLed32KbpsColor = nil;
static UIColor* _btnLed64KbpsColor = nil;
static UIColor* _btnLed128KbpsColor = nil;
static UIColor* _btnLed256KbpsColor = nil;
static UIColor* _btnLedAirplayColor = nil;

#pragma mark Initialization

+ (void)initialize
{
    // Colors Initialization
    _btnLed32KbpsColor = [UIColor colorWithRed: 0.992 green: 0.482 blue: 0.267 alpha: 1];
    _btnLed64KbpsColor = [UIColor colorWithRed: 0.369 green: 0.925 blue: 0.443 alpha: 1];
    _btnLed128KbpsColor = [UIColor colorWithRed: 0.984 green: 0.239 blue: 0.859 alpha: 1];
    _btnLed256KbpsColor = [UIColor colorWithRed: 0.224 green: 0.776 blue: 0.988 alpha: 1];
    _btnLedAirplayColor = [UIColor colorWithRed: 0.302 green: 0.902 blue: 0.953 alpha: 1];

}

#pragma mark Colors

+ (UIColor*)btnLed32KbpsColor { return _btnLed32KbpsColor; }
+ (UIColor*)btnLed64KbpsColor { return _btnLed64KbpsColor; }
+ (UIColor*)btnLed128KbpsColor { return _btnLed128KbpsColor; }
+ (UIColor*)btnLed256KbpsColor { return _btnLed256KbpsColor; }
+ (UIColor*)btnLedAirplayColor { return _btnLedAirplayColor; }

#pragma mark Drawing Methods

+ (void)drawRetroBtnWithLedColor: (UIColor*)ledColor highlighted: (BOOL)highlighted enabled: (BOOL)enabled;
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Color Declarations
    CGFloat ledColorHSBA[4];
    [ledColor getHue: &ledColorHSBA[0] saturation: &ledColorHSBA[1] brightness: &ledColorHSBA[2] alpha: &ledColorHSBA[3]];

    UIColor* btn_led_outer_c = [UIColor colorWithHue: ledColorHSBA[0] saturation: 1 brightness: ledColorHSBA[2] alpha: ledColorHSBA[3]];
    CGFloat btn_led_outer_cRGBA[4];
    [btn_led_outer_c getRed: &btn_led_outer_cRGBA[0] green: &btn_led_outer_cRGBA[1] blue: &btn_led_outer_cRGBA[2] alpha: &btn_led_outer_cRGBA[3]];

    UIColor* btn_led_outer_enabled_c = [UIColor colorWithRed: (btn_led_outer_cRGBA[0] * 0.6 + 0.4) green: (btn_led_outer_cRGBA[1] * 0.6 + 0.4) blue: (btn_led_outer_cRGBA[2] * 0.6 + 0.4) alpha: (btn_led_outer_cRGBA[3] * 0.6 + 0.4)];
    UIColor* ledDisabledColor = [UIColor colorWithHue: ledColorHSBA[0] saturation: ledColorHSBA[1] brightness: 0.7 alpha: ledColorHSBA[3]];

    //// Shadow Declarations
    UIColor* btn_led_inner = [UIColor.whiteColor colorWithAlphaComponent: 0.52];
    CGSize btn_led_innerOffset = CGSizeMake(0.1, -0.1);
    CGFloat btn_led_innerBlurRadius = 2;
    UIColor* btn_led_outer_enabled = btn_led_outer_enabled_c;
    CGSize btn_led_outer_enabledOffset = CGSizeMake(0.1, -0.1);
    CGFloat btn_led_outer_enabledBlurRadius = 10;

    //// Image Declarations
    UIImage* retro_btn_bg = [UIImage imageNamed: @"retro_btn_bg.png"];
    UIImage* retro_btn_shadow = [UIImage imageNamed: @"retro_btn_shadow.png"];

    //// Variable Declarations
    CGFloat retro_btn_shadow_alpha = highlighted ? 0.4 : 1;
    CGFloat retro_btn_scale = highlighted ? 0.96 : 1;
    BOOL disabled = !enabled;

    //// btn
    {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 35.4, 16.88);
        CGContextScaleCTM(context, retro_btn_scale, retro_btn_scale);



        //// bg_shadow
        {
            CGContextSaveGState(context);
            CGContextSetAlpha(context, retro_btn_shadow_alpha);
            CGContextBeginTransparencyLayer(context, NULL);


            //// Rectangle 2 Drawing
            UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-33, -15, 66, 30)];
            CGContextSaveGState(context);
            [rectangle2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-33, 15, retro_btn_shadow.size.width, retro_btn_shadow.size.height), retro_btn_shadow.CGImage);
            CGContextRestoreGState(context);


            CGContextEndTransparencyLayer(context);
            CGContextRestoreGState(context);
        }


        //// bg Drawing
        UIBezierPath* bgPath = [UIBezierPath bezierPathWithRect: CGRectMake(-35.4, -16.88, 70, 22)];
        CGContextSaveGState(context);
        [bgPath addClip];
        [retro_btn_bg drawInRect: CGRectMake(-35, -17, retro_btn_bg.size.width, retro_btn_bg.size.height)];
        CGContextRestoreGState(context);


        //// led
        {
            if (enabled)
            {
                //// led_enabled
                {
                    //// Rectangle Drawing
                    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(-26.4, -11.88, 4, 7) cornerRadius: 1];
                    CGContextSaveGState(context);
                    CGContextSetShadowWithColor(context, btn_led_outer_enabledOffset, btn_led_outer_enabledBlurRadius, [btn_led_outer_enabled CGColor]);
                    [ledColor setFill];
                    [rectanglePath fill];

                    ////// Rectangle Inner Shadow
                    CGContextSaveGState(context);
                    UIRectClip(rectanglePath.bounds);
                    CGContextSetShadowWithColor(context, CGSizeZero, 0, NULL);

                    CGContextSetAlpha(context, CGColorGetAlpha([btn_led_inner CGColor]));
                    CGContextBeginTransparencyLayer(context, NULL);
                    {
                        UIColor* opaqueShadow = [btn_led_inner colorWithAlphaComponent: 1];
                        CGContextSetShadowWithColor(context, btn_led_innerOffset, btn_led_innerBlurRadius, [opaqueShadow CGColor]);
                        CGContextSetBlendMode(context, kCGBlendModeSourceOut);
                        CGContextBeginTransparencyLayer(context, NULL);

                        [opaqueShadow setFill];
                        [rectanglePath fill];

                        CGContextEndTransparencyLayer(context);
                    }
                    CGContextEndTransparencyLayer(context);
                    CGContextRestoreGState(context);

                    CGContextRestoreGState(context);

                }
            }


            if (disabled)
            {
                //// led_disabled
                {
                    CGContextSaveGState(context);
                    CGContextSetAlpha(context, 0.8);
                    CGContextBeginTransparencyLayer(context, NULL);


                    //// Rectangle 3 Drawing
                    UIBezierPath* rectangle3Path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(-26.4, -11.88, 4, 7) cornerRadius: 1];
                    [ledDisabledColor setFill];
                    [rectangle3Path fill];


                    CGContextEndTransparencyLayer(context);
                    CGContextRestoreGState(context);
                }
            }
        }



        CGContextRestoreGState(context);
    }
}

+ (void)drawOnairTimelineWithArtistTitleText: (NSString*)artistTitleText timeLineProgress: (CGFloat)timeLineProgress;
{
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Color Declarations
    UIColor* txtColor = [UIColor colorWithRed: 0.29 green: 0.267 blue: 0.235 alpha: 1];
    UIColor* lightGradientColor = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0];
    UIColor* lightGradientColor2 = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 0.5];

    //// Gradient Declarations
    CGFloat lightGradientLocations[] = {0.48, 0.48};
    CGGradientRef lightGradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)@[(id)lightGradientColor2.CGColor, (id)lightGradientColor.CGColor], lightGradientLocations);

    //// Image Declarations
    UIImage* img_01 = [UIImage imageNamed: @"img_01.png"];
    UIImage* img_02 = [UIImage imageNamed: @"img_02.png"];
    UIImage* img_03 = [UIImage imageNamed: @"img_03.png"];
    UIImage* img_04 = [UIImage imageNamed: @"img_04.png"];

    //// Variable Declarations
    CGFloat progressX = timeLineProgress * 388 / 2.0 + 65;

    //// Rectangle 3 Drawing
    UIBezierPath* rectangle3Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 320, 54)];
    CGContextSaveGState(context);
    [rectangle3Path addClip];
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawTiledImage(context, CGRectMake(0, 0, img_04.size.width, img_04.size.height), img_04.CGImage);
    CGContextRestoreGState(context);


    //// Progress Point Drawing
    UIBezierPath* progressPointPath = [UIBezierPath bezierPathWithRect: CGRectMake(progressX, 11, 5, 46)];
    CGContextSaveGState(context);
    [progressPointPath addClip];
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawTiledImage(context, CGRectMake(65, -11, img_03.size.width, img_03.size.height), img_03.CGImage);
    CGContextRestoreGState(context);


    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 320, 15)];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawTiledImage(context, CGRectMake(0, 0, img_01.size.width, img_01.size.height), img_01.CGImage);
    CGContextRestoreGState(context);


    //// Rectangle 2 Drawing
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 51, 320, 15)];
    CGContextSaveGState(context);
    [rectangle2Path addClip];
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawTiledImage(context, CGRectMake(0, -51, img_02.size.width, img_02.size.height), img_02.CGImage);
    CGContextRestoreGState(context);


    //// Text Drawing
    CGRect textRect = CGRectMake(129, 23, 64, 8);
    NSMutableParagraphStyle* textStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
    textStyle.alignment = NSTextAlignmentCenter;

    NSDictionary* textFontAttributes = @{NSFontAttributeName: [UIFont fontWithName: @"Helvetica" size: 7], NSForegroundColorAttributeName: txtColor, NSParagraphStyleAttributeName: textStyle};

    [@"В ЭФИРЕ" drawInRect: textRect withAttributes: textFontAttributes];


    //// Text 2 Drawing
    CGRect text2Rect = CGRectMake(29, 33, 263, 13);
    NSMutableParagraphStyle* text2Style = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
    text2Style.alignment = NSTextAlignmentCenter;

    NSDictionary* text2FontAttributes = @{NSFontAttributeName: [UIFont fontWithName: @"Helvetica" size: 12], NSForegroundColorAttributeName: txtColor, NSParagraphStyleAttributeName: text2Style};

    [artistTitleText drawInRect: text2Rect withAttributes: text2FontAttributes];


    //// Group
    {
        CGContextSaveGState(context);
        CGContextSetAlpha(context, 0.5);
        CGContextSetBlendMode(context, kCGBlendModeScreen);
        CGContextBeginTransparencyLayer(context, NULL);


        //// Rectangle 5 Drawing
        UIBezierPath* rectangle5Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 320, 66)];
        CGContextSaveGState(context);
        [rectangle5Path addClip];
        CGContextDrawLinearGradient(context, lightGradient, CGPointMake(63.5, -63.5), CGPointMake(256.5, 129.5), 0);
        CGContextRestoreGState(context);


        CGContextEndTransparencyLayer(context);
        CGContextRestoreGState(context);
    }


    //// Cleanup
    CGGradientRelease(lightGradient);
    CGColorSpaceRelease(colorSpace);
}

+ (void)drawSignalHUDWithSignalQuality: (CGFloat)signalQuality;
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();

    //// Color Declarations
    UIColor* txtColor = [UIColor colorWithRed: 0.29 green: 0.267 blue: 0.235 alpha: 1];

    //// Shadow Declarations
    UIColor* shadow = [UIColor.blackColor colorWithAlphaComponent: 0.53];
    CGSize shadowOffset = CGSizeMake(2.1, -1.1);
    CGFloat shadowBlurRadius = 1;

    //// Image Declarations
    UIImage* signalHUDbg = [UIImage imageNamed: @"signalHUDbg.png"];

    //// Variable Declarations
    CGFloat signalPointAngle = signalQuality * -92 + 46;

    //// Signal HUD bg Rect Drawing
    UIBezierPath* signalHUDBgRectPath = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 93, 64)];
    CGContextSaveGState(context);
    [signalHUDBgRectPath addClip];
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawTiledImage(context, CGRectMake(0, 0, signalHUDbg.size.width, signalHUDbg.size.height), signalHUDbg.CGImage);
    CGContextRestoreGState(context);


    //// Group
    {
        CGContextSaveGState(context);
        CGContextBeginTransparencyLayer(context, NULL);

        //// Clip clip
        UIBezierPath* clipPath = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 93, 62.5)];
        [clipPath addClip];


        //// Bezier Drawing
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 46.5, 66.5);
        CGContextRotateCTM(context, -signalPointAngle * M_PI / 180);

        UIBezierPath* bezierPath = UIBezierPath.bezierPath;
        [bezierPath moveToPoint: CGPointMake(0, -52)];
        [bezierPath addLineToPoint: CGPointMake(0, 0)];
        [UIColor.redColor setFill];
        [bezierPath fill];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, [shadow CGColor]);
        [UIColor.whiteColor setStroke];
        bezierPath.lineWidth = 1;
        [bezierPath stroke];
        CGContextRestoreGState(context);

        CGContextRestoreGState(context);


        //// Text 2 Drawing
        CGRect text2Rect = CGRectMake(31, 31, 31, 9);
        NSMutableParagraphStyle* text2Style = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
        text2Style.alignment = NSTextAlignmentCenter;

        NSDictionary* text2FontAttributes = @{NSFontAttributeName: [UIFont fontWithName: @"Helvetica" size: 5], NSForegroundColorAttributeName: txtColor, NSParagraphStyleAttributeName: text2Style};

        [@"СИГНАЛ" drawInRect: text2Rect withAttributes: text2FontAttributes];


        CGContextEndTransparencyLayer(context);
        CGContextRestoreGState(context);
    }


    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 92.5, 7)];
    [UIColor.blackColor setFill];
    [rectanglePath fill];


    //// Rectangle 2 Drawing
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 56, 92.5, 7)];
    [UIColor.blackColor setFill];
    [rectangle2Path fill];
}

+ (void)drawVolumeThumbWithFrame: (CGRect)frame enabled: (BOOL)enabled thumbAngle: (CGFloat)thumbAngle lightAngle: (CGFloat)lightAngle playBtnPressed: (BOOL)playBtnPressed width: (CGFloat)width;
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();


    //// Image Declarations
    UIImage* volume_bnt_bg = [UIImage imageNamed: @"volume_bnt_bg.png"];
    UIImage* volume_btn_bg_texture = [UIImage imageNamed: @"volume_btn_bg_texture.png"];
    UIImage* volume_btn_icon_play = [UIImage imageNamed: @"volume_btn_icon_play.png"];
    UIImage* volume_btn_normal_outer = [UIImage imageNamed: @"volume_btn_normal_outer.png"];
    UIImage* volume_btn_presstd_inner = [UIImage imageNamed: @"volume_btn_presstd_inner.png"];
    UIImage* volume_thumb_bg = [UIImage imageNamed: @"volume_thumb_bg.png"];
    UIImage* volume_thumb_blicks = [UIImage imageNamed: @"volume_thumb_blicks.png"];
    UIImage* volume_thumb_pin = [UIImage imageNamed: @"volume_thumb_pin.png"];
    UIImage* volume_btn_icon_pause = [UIImage imageNamed: @"volume_btn_icon_pause.png"];

    //// Variable Declarations
    BOOL disabled = !enabled;
    BOOL playBtnNormal = !playBtnPressed;
    CGFloat playBtnIconScale = playBtnPressed ? 0.95 : 1;
    CGFloat scale = width / 148.0;

    //// Group 4
    {
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, CGRectGetMinX(frame) + 0.50338 * CGRectGetWidth(frame), CGRectGetMinY(frame) + 0.48026 * CGRectGetHeight(frame));
        CGContextScaleCTM(context, scale, scale);



        //// volume_thumb_bg 2 Drawing
        UIBezierPath* volume_thumb_bg2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-73.5, -72, 147, 151)];
        CGContextSaveGState(context);
        [volume_thumb_bg2Path addClip];
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawTiledImage(context, CGRectMake(-73, 72, volume_thumb_bg.size.width, volume_thumb_bg.size.height), volume_thumb_bg.CGImage);
        CGContextRestoreGState(context);


        //// volume_btn_bg_texture 2 Drawing
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, -0.5, 0.5);
        CGContextRotateCTM(context, -thumbAngle * M_PI / 180);

        UIBezierPath* volume_btn_bg_texture2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-59, -59, 118, 117)];
        CGContextSaveGState(context);
        [volume_btn_bg_texture2Path addClip];
        [volume_btn_bg_texture drawInRect: CGRectMake(-59, -59, volume_btn_bg_texture.size.width, volume_btn_bg_texture.size.height)];
        CGContextRestoreGState(context);

        CGContextRestoreGState(context);


        if (playBtnNormal)
        {
            //// volume_btn_normal_outer 2 Drawing
            UIBezierPath* volume_btn_normal_outer2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-30.5, -29, 62, 61)];
            CGContextSaveGState(context);
            [volume_btn_normal_outer2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-30, 29, volume_btn_normal_outer.size.width, volume_btn_normal_outer.size.height), volume_btn_normal_outer.CGImage);
            CGContextRestoreGState(context);
        }


        //// volume_bnt_bg 2 Drawing
        UIBezierPath* volume_bnt_bg2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-28.5, -27, 58, 57)];
        CGContextSaveGState(context);
        [volume_bnt_bg2Path addClip];
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawTiledImage(context, CGRectMake(-28, 27, volume_bnt_bg.size.width, volume_bnt_bg.size.height), volume_bnt_bg.CGImage);
        CGContextRestoreGState(context);


        if (disabled)
        {
            //// volume_btn_icon_play 2 Drawing
            CGContextSaveGState(context);
            CGContextScaleCTM(context, playBtnIconScale, playBtnIconScale);

            UIBezierPath* volume_btn_icon_play2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-7.5, -11, 21, 24)];
            CGContextSaveGState(context);
            [volume_btn_icon_play2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-7, 11, volume_btn_icon_play.size.width, volume_btn_icon_play.size.height), volume_btn_icon_play.CGImage);
            CGContextRestoreGState(context);

            CGContextRestoreGState(context);
        }


        if (enabled)
        {
            //// volume_btn_icon_pause 2 Drawing
            CGContextSaveGState(context);
            CGContextTranslateCTM(context, 0, 1);
            CGContextScaleCTM(context, playBtnIconScale, playBtnIconScale);

            UIBezierPath* volume_btn_icon_pause2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-8.5, -12, 17, 24)];
            CGContextSaveGState(context);
            [volume_btn_icon_pause2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-8, 12, volume_btn_icon_pause.size.width, volume_btn_icon_pause.size.height), volume_btn_icon_pause.CGImage);
            CGContextRestoreGState(context);

            CGContextRestoreGState(context);
        }


        //// Group
        {
            CGContextSaveGState(context);
            CGContextRotateCTM(context, -lightAngle * M_PI / 180);

            CGContextSetBlendMode(context, kCGBlendModeOverlay);
            CGContextBeginTransparencyLayer(context, NULL);


            //// volume_thumb_blicks 2 Drawing
            CGContextSaveGState(context);

            UIBezierPath* volume_thumb_blicks2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-59, -58.5, 119, 118)];
            CGContextSaveGState(context);
            [volume_thumb_blicks2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-59, 58, volume_thumb_blicks.size.width, volume_thumb_blicks.size.height), volume_thumb_blicks.CGImage);
            CGContextRestoreGState(context);

            CGContextRestoreGState(context);


            CGContextEndTransparencyLayer(context);

            CGContextRestoreGState(context);
        }


        if (playBtnPressed)
        {
            //// Group 2
            {
                //// volume_btn_presstd_inner 2 Drawing
                UIBezierPath* volume_btn_presstd_inner2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-29.5, -29, 60, 60)];
                CGContextSaveGState(context);
                [volume_btn_presstd_inner2Path addClip];
                CGContextScaleCTM(context, 1.0, -1.0);
                CGContextDrawTiledImage(context, CGRectMake(-29, 29, volume_btn_presstd_inner.size.width, volume_btn_presstd_inner.size.height), volume_btn_presstd_inner.CGImage);
                CGContextRestoreGState(context);
            }
        }


        //// Group 3
        {
            CGContextSaveGState(context);
            CGContextRotateCTM(context, -thumbAngle * M_PI / 180);



            //// volume_thumb_pin 2 Drawing
            CGContextSaveGState(context);
            CGContextRotateCTM(context, -41 * M_PI / 180);

            UIBezierPath* volume_thumb_pin2Path = [UIBezierPath bezierPathWithRect: CGRectMake(-37.5, 39, 4, 4)];
            CGContextSaveGState(context);
            [volume_thumb_pin2Path addClip];
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawTiledImage(context, CGRectMake(-37, -39, volume_thumb_pin.size.width, volume_thumb_pin.size.height), volume_thumb_pin.CGImage);
            CGContextRestoreGState(context);

            CGContextRestoreGState(context);



            CGContextRestoreGState(context);
        }



        CGContextRestoreGState(context);
    }
}

#pragma mark Generated Images

+ (UIImage*)imageOfRetroBtnWithLedColor: (UIColor*)ledColor highlighted: (BOOL)highlighted enabled: (BOOL)enabled;
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(70, 31), NO, 0.0f);
    [RFMStyleKit drawRetroBtnWithLedColor: ledColor highlighted: highlighted enabled: enabled];
    UIImage* imageOfRetroBtn = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return imageOfRetroBtn;
}

@end
