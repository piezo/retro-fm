//
//  RFMVolumeThumb.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/18/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMVolumeThumb.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

CGFloat CGPointToDegree(CGPoint point) {
    // Provides a directional bearing from (0,0) to the given point.
    // standard cartesian plain coords: X goes up, Y goes right
    // result returns degrees, -180 to 180 ish: 0 degrees = up, -90 = left, 90 = right
    CGFloat bearingRadians = atan2f(point.y, point.x);
    CGFloat bearingDegrees = bearingRadians * (180. / M_PI);
    return bearingDegrees;

}

@implementation RFMVolumeThumb{
    RACCommand *_racCommand;
}

static CMMotionManager *motionManager = nil;
static NSOperationQueue *accelerometerQueue = nil;
static CGFloat imageRotation = 0.0f;
static NSInteger touchType = 0;

-(void)dealloc{
    

    
}

-(void)awakeFromNib{
    @weakify(self)
    
    motionManager = [[CMMotionManager alloc] init];
    motionManager.accelerometerUpdateInterval = 0.01f;
    accelerometerQueue = [[NSOperationQueue alloc] init];
    
    
//    [motionManager startAccelerometerUpdatesToQueue:accelerometerQueue
//                                        withHandler:^(CMAccelerometerData *accelerometerData,
//                                                      NSError *error){
//                                            
//                                            @strongify(self);
//                                            imageRotation = (imageRotation * 0.9f) + (accelerometerData.acceleration.x * M_PI_2) * 0.1f;
//                                            
//                                            
////                                            self.lightAngle = RADIANS_TO_DEGREES(imageRotation);
//                                            
//                                        }];
    
    
    [[RACSignal merge:@[
                       RACObserve(self, lightAngle),
                       RACObserve(self, rotationAngle),
                       RACObserve(self, highlighted),
                       RACObserve(self, selected),
                       ]]
     subscribeNext:^(id x) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self);
            [self setNeedsDisplay];
        });

    }];
    
    [RACObserve(self, volume) subscribeNext:^(id x) {
        @strongify(self);
        self.rotationAngle = (1-[x floatValue])*270+45;
        [self setNeedsDisplay];
    }];
    
    
    self.layer.contentsScale=.3;
    self.backgroundColor=[UIColor clearColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    
    [RFMStyleKit drawVolumeThumbWithFrame:rect enabled:_selected thumbAngle:_rotationAngle lightAngle:_lightAngle playBtnPressed:_highlighted width:rect.size.width];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint currLoc = [touch locationInView:self];
    
    CGRect r = CGRectInset(self.bounds, self.bounds.size.width*45/127, self.bounds.size.width*45/127);
    
    touchType = CGRectContainsPoint(r, currLoc)?0:1;
    if(touchType == 0){
        _highlighted = YES;
        [self setNeedsDisplay];
    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touchType == 1) {
        UITouch *touch = [touches anyObject];
        CGPoint prevLoc = [touch previousLocationInView:self];
        CGPoint currLoc = [touch locationInView:self];
        
        prevLoc = CGPointMake(prevLoc.x-self.frame.size.width/2, prevLoc.y-self.frame.size.height/2);
        currLoc = CGPointMake(currLoc.x-self.frame.size.width/2, currLoc.y-self.frame.size.height/2);
        
        CGFloat prevAngle = -(CGPointToDegree(prevLoc)-90);
        CGFloat currAngle = -(CGPointToDegree(currLoc)-90);
        
        CGFloat _rotAngle = self.rotationAngle - (prevAngle-currAngle);
        
        
        
        _rotAngle = (int)(_rotAngle+3600)%360;
        _rotAngle = (_rotAngle>315)?315:_rotAngle;
        _rotAngle = (_rotAngle<45)?45:_rotAngle;
        
        self.volume = 1-(_rotAngle-45)/270;
    }
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touchType==0) {
        
        UITouch *touch = [touches anyObject];
        CGPoint currLoc = [touch locationInView:self];
        
        CGRect r = CGRectInset(self.bounds, self.bounds.size.width*45/127, self.bounds.size.width*45/127);
        
        if (CGRectContainsPoint(r, currLoc)) {
            _selected = !_selected;
            [self playBtnDidPress];
            [self setNeedsDisplay];
        }
        
        _highlighted = NO;
        [self setNeedsDisplay];
    }
}

- (CGFloat) pointPairToBearingDegrees:(CGPoint)startingPoint secondPoint:(CGPoint) endingPoint
{
    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start
    float bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in radians
    float bearingDegrees = bearingRadians * (180.0 / M_PI); // convert to degrees
    bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
    return bearingDegrees;
}

-(void) playBtnDidPress{
    
}


@end
