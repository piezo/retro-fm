//
//  UILabel+CustomFont.m
//  Seat Sound
//
//  Created by Oleksandr Izvekov on 4/20/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "UILabel+CustomFont.h"

@implementation UILabel (CustomFont)

- (NSString *)fontName {
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end
