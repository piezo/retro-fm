//
//  UILabel+CustomFont.h
//  Seat Sound
//
//  Created by Oleksandr Izvekov on 4/20/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (CustomFont)

@property (nonatomic, copy) NSString* fontName;

@end
