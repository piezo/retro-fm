//
//  RFMAppDelegate.m
//  Retro FM
//
//  Created by Oleksandr Izvekov on 8/17/14.
//  Copyright (c) 2014 Piezo. All rights reserved.
//

#import "RFMAppDelegate.h"
#import <Crashlytics/Crashlytics.h>

@implementation RFMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self setupAudioSession];
    [[RFMCommonData shared] startPlaylistUpdate];
    
    [Crashlytics startWithAPIKey:@"1f0ebbe7f8f4b4ab27c87351d010c8bf94642f64"];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)remoteControlReceivedWithEvent:(UIEvent *)event{
    if (event.type == UIEventTypeRemoteControl){
        switch (event.subtype) {
            case UIEventSubtypeRemoteControlTogglePlayPause:{
                
                if ([[[RFMCommonData shared] player] state] == STKAudioPlayerStatePlaying) {
                    [[RFMCommonData shared] stop];
                }else{
                    [[RFMCommonData shared] play];
                    
                    
                }
                
                break;
            }
                
            case UIEventSubtypeRemoteControlPlay:{
                [[RFMCommonData shared] play];
                break;
            }
                
            case UIEventSubtypeRemoteControlPause:{
                [[RFMCommonData shared] stop];
                                
                break;
            }
                
                
            default:
                break;
        }
    }
}

- (void)setupAudioSession
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    

    

}

@end
